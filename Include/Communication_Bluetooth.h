

#ifndef __COMMUNICATION_BLUETOOTH_H
#define __COMMUNICATION_BLUETOOTH_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "nrf_sdh_ble.h"
		#include "nrf_ble_scan.h"
		#include "nrf_ble_gatt.h"
		#include "ble_advertising.h"
		#include "app_timer.h"

		/* GAP */
		void GAP_Initialization(void);

		/* GATT */
		void GATT_Initialization(void);		
		
		/* Bluetooth */
		#define APP_BLE_CONN_CFG_TAG      1                                     /**< Tag that refers to the BLE stack configuration that is set with @ref sd_ble_cfg_set. The default tag is @ref APP_BLE_CONN_CFG_TAG. */
		#define APP_BLE_OBSERVER_PRIO     3                                     /**< BLE observer priority of the application. There is no need to modify this value. */
	
		#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to the first time sd_ble_gap_conn_param_update is called (5 seconds). */
		#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
		#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

		#define APP_ADV_INTERVAL                300                                         /**< The advertising interval (in units of 0.625 ms). This value corresponds to 187.5 ms. */
		#define APP_ADV_DURATION                18000                                       /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

		#define ManufacSpecPayload_NoUseOffset 7																/* Unused Information length of Manufacturer Specific Payload */
		#define MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH 		4								/* Major and Minor data length of Bluetooth definition */
		#define MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH 				4								/* AEON TPMS Device ID length */

		typedef struct 
		{
			uint8_t DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Temperature;
			uint8_t Pressure;
		}MANUFACTURER_DATA_RECEIVE;
		
		typedef struct
		{
			uint8_t ManufacSpecDataPackagingFinish_Flag : 1;
		}COMMUNICATIONFLAG;
			
		void BLE_ProtocolStack_Initialization(void);
		void Connect_Initialization(void);
		void Advertising_Initialization(void);
		void BLE_Scan_Initialization(void);
		void BLE_AdvertisingAndScanning_Start(void);
		
		static void ScanEvent_handler(scan_evt_t const * p_scan_evt);
		static void BLEEvent_handler(ble_evt_t const * p_ble_evt, void * p_context);
		static void conn_params_error_handler(uint32_t nrf_error);
		static void on_adv_evt(ble_adv_evt_t ble_adv_evt);
		
#ifdef __cplusplus
}
#endif
#endif /*__COMMUNICATION_BLUETOOTH_H */



#ifndef __COMMUNICATION_UART_H
#define __COMMUNICATION_UART_H
#ifdef __cplusplus
 extern "C" {
#endif
		
		#include <stdint.h>
		#include "app_uart.h"
		#include "ble_nus.h"
		
				/* UART */
		#define UART_TX_BUF_SIZE          256                                   /**< UART TX buffer size. */
		#define UART_RX_BUF_SIZE          256                                   /**< UART RX buffer size. */

		void UART_Initialization(void);
		static void UARTEvent_handler(app_uart_evt_t * p_event);
		
		/* NUSc */
		#define ECHOBACK_BLE_UART_DATA  1                                       /**< Echo the UART data that is received over the Nordic UART Service (NUS) back to the sender. */

		void BluetoothServices_Initialization(void);
		void NRF_BLE_ConnectHandler_QueueWrite(void);
		static void nrf_qwr_error_handler(uint32_t nrf_error);
		static void NUS_Data_handler(ble_nus_evt_t * p_evt);		
		
#ifdef __cplusplus
}
#endif
#endif /*__COMMUNICATION_UART_H */



#ifndef __WHITELIST_H
#define __WHITELIST_H
#ifdef __cplusplus
 extern "C" {
#endif

		#include <stdint.h>
		#include "Communication_Bluetooth.h"
		
		#define IMPORT_WHITELIST_ARRAY_RAW_NUMBER					3
		
		typedef struct 
		{
			uint8_t DeviceAddress[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
		}WhiteListImport_t;
		
		struct AEON_TPMS_LinkList
		{
			uint8_t DeviceID[MANUFACTURER_DEFINITION_DEVICE_ID_LENGTH];
			uint8_t Temperature;
			uint8_t Pressure;
			struct AEON_TPMS_LinkList *p_NextNode;
		};
		
		uint32_t BeaconWhiteList_Set(WhiteListImport_t *p_ListImport);
		uint32_t BeaconWhiteList_Matching(MANUFACTURER_DATA_RECEIVE *p_manufacData);
		
		void BeaconWhiteList_Display(void);
		void BeaconWhiteList_Release(void);
		
#ifdef __cplusplus
}
#endif
#endif /*__WHITELIST_H */



#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
//#include "nrf_ble_qwr.h"
#include "nrf_fstorage.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "bsp_btn_ble.h"

#include "Communication_Bluetooth.h"
#include "Communication_UART.h"
#include "WhiteList.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define DEVICE_NAME                     "AEON_VCU_Bluetooth"            /**< Name of device used for advertising. */

NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. 		 */
BLE_ADVERTISING_DEF(m_advertising);                                 		/**< Advertising module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                               /**< Scanning Module instance. */

MANUFACTURER_DATA_RECEIVE ManuFacDataRx;
COMMUNICATIONFLAG CommunicFlag;

extern uint16_t m_conn_handle;

static uint8_t m_target_manufacturer_specific_data[NRF_BLE_ADV_MANUFACTURER_SPECIFIC_DATA_LENGTH] = { 
																				0x02, 0x15, 
																				0xB5, 0x4A, 0xDC, 0x00, 0x67, 0xF9, 0x11, 0xD9,
																				0x96, 0x69, 0x08, 0x00, 0x20, 0x0C, 0x9A, 0x66																			
																				};
																				
/**< Scan parameters requested for scanning and connection. */
static ble_gap_scan_params_t m_scan_param =
{
        .active        = 0,           /* Disable the acvtive scanning */
        .interval      = NRF_BLE_SCAN_SCAN_INTERVAL,
        .window        = NRF_BLE_SCAN_SCAN_WINDOW,
        .filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
        .timeout       = 0,
        .scan_phys     = BLE_GAP_PHY_1MBPS,
};

/**@brief UUIDs that the central application scans for if the name above is set to an empty string,
 * and that are to be advertised by the peripherals.
 */
static ble_uuid_t m_adv_uuids[] =
{
    {BLE_UUID_NUS_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN}
};

/******************************************************************************/
/*                 	  	 	 GAP Function subroutine  		                	  	*/
/******************************************************************************/
/**@brief Function for initializing the GAP.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device, including the device name, appearance, and the preferred connection parameters.
 */
void GAP_Initialization(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = m_scan.conn_params.min_conn_interval;
    gap_conn_params.max_conn_interval = m_scan.conn_params.max_conn_interval;
    gap_conn_params.slave_latency     = m_scan.conn_params.slave_latency;
    gap_conn_params.conn_sup_timeout  = m_scan.conn_params.conn_sup_timeout;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/******************************************************************************/
/*                 	  	 	 GATT Function subroutine 		                	  	*/
/******************************************************************************/
/**@brief Function for initializing the GATT library. */
void GATT_Initialization(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}

/******************************************************************************/
/*                 	  	 Bluetooth Function subroutine	                	  	*/
/******************************************************************************/
/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupts.
 */
void BLE_ProtocolStack_Initialization(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, BLEEvent_handler, NULL);
}

/**@brief Function for initializing the Connection Parameters module.
 */
void Connect_Initialization(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_CONN_HANDLE_INVALID; // Start upon connection.
    cp_init.disconnect_on_fail             = true;
    cp_init.evt_handler                    = NULL;  // Ignore events.
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the advertising functionality.
 */
void Advertising_Initialization(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

/**@brief Function for initializing the scanning and setting the filters.
 */
void BLE_Scan_Initialization(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

		init_scan.p_scan_param     = &m_scan_param;
    init_scan.connect_if_match = false;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, ScanEvent_handler);
    APP_ERROR_CHECK(err_code);

		err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_MANUFACTURER_FILTER, m_target_manufacturer_specific_data);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_MANUFACTURER_SPECIFIC_FILTER, false);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting scanning. */
void BLE_AdvertisingAndScanning_Start(void)
{
  ret_code_t err_code;
	
	/* check if there are no flash operations in progress */
	if (!nrf_fstorage_is_busy(NULL))
	{
		/* Start scanning for peripherals and initiate connection to devices which
       advertise Heart Rate or Running speed and cadence UUIDs. */
    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);
	
		/* Start advertising. */
    err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
    APP_ERROR_CHECK(err_code);
	}
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void BLEEvent_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t            err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");

            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            NRF_BLE_ConnectHandler_QueueWrite();
				
						/* LED indication status change */
						err_code = bsp_indication_set(BSP_INDICATE_SCANNING);
						APP_ERROR_CHECK(err_code);
				
						bsp_board_led_on(BSP_BOARD_LED_1);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
        
						/* LED indication status change */
						bsp_board_led_on(BSP_BOARD_LED_1);
				
						err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
						APP_ERROR_CHECK(err_code);
				
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}

static void ScanEvent_handler(scan_evt_t const * p_scan_evt)
{
		ble_gap_evt_adv_report_t const * p_adv_report = p_scan_evt->params.filter_match.p_adv_report;
	
		uint8_t DeviceAdverAddr_Array[BLE_GAP_ADDR_LEN] = { 0 };
		uint8_t ManufacSpecMajorMinor_Array[MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH] = { 0 };
	
    switch(p_scan_evt->scan_evt_id)
    {
			case NRF_BLE_SCAN_EVT_FILTER_MATCH:
				NRF_LOG_INFO("NRF_BLE_SCAN_EVT_FILTER_MATCH");
				
				/* Advertising Address */ 
				memcpy(DeviceAdverAddr_Array, &p_adv_report -> peer_addr.addr, BLE_GAP_ADDR_LEN);
//				NRF_LOG_RAW_INFO("Advertising Address : %02X %02X %02X %02X %02X %02X \r\n", DeviceAdverAddr_Array[0], 
//																																										 DeviceAdverAddr_Array[1],
//				 																																						 DeviceAdverAddr_Array[2],
//																																										 DeviceAdverAddr_Array[3],
//																																										 DeviceAdverAddr_Array[4],
//																																										 DeviceAdverAddr_Array[5]);
				
				/* Beacon Major & Minor */
				memcpy(ManufacSpecMajorMinor_Array, m_scan.scan_buffer.p_data + 
																						NRF_BLE_ADV_MANUFACTURER_SPECIFIC_DATA_LENGTH +
																						ManufacSpecPayload_NoUseOffset,
																						MANUFACTURER_SPECIFICATION_MAJORMINOR_LENGTH);
			
				/* Loading Data */
				ManuFacDataRx.DeviceID[0] = DeviceAdverAddr_Array[0];
				ManuFacDataRx.DeviceID[1] = (ManufacSpecMajorMinor_Array[2] & 0x80) >> 7;
				ManuFacDataRx.DeviceID[2] = ManufacSpecMajorMinor_Array[0];
				ManuFacDataRx.DeviceID[3] = ManufacSpecMajorMinor_Array[1];
				
				ManuFacDataRx.Temperature = ManufacSpecMajorMinor_Array[2] & 0x7F;
				ManuFacDataRx.Pressure		 = ManufacSpecMajorMinor_Array[3];
				
//				NRF_LOG_RAW_INFO("Device ID : %02X %02X %02X %02X \r\n", ManuFacDataRx.DeviceID[0],
//																																 ManuFacDataRx.DeviceID[1],
//																																 ManuFacDataRx.DeviceID[2],
//																																 ManuFacDataRx.DeviceID[3]);				
//				NRF_LOG_RAW_INFO("Temperature : %02X \r\n", ManuFacDataRx.Temperature);
//				NRF_LOG_RAW_INFO("Pressure : %02X \r\n", ManuFacDataRx.Pressure);	
//				NRF_LOG_RAW_INFO ("==================================\r\n");
				
				CommunicFlag.ManufacSpecDataPackagingFinish_Flag = 1;
				break;
			
			default:
				break;
				
    }
}

/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for handling advertising events.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
        {
            NRF_LOG_INFO("Fast advertising.");
        } break;

        case BLE_ADV_EVT_IDLE:
        {
            ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            // No implementation needed.
            break;
    }
}
